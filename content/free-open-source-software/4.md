---
weight: 5
---
{{% section %}}
## Free Software

> "this is a matter of freedom, not price, so think of 'free speech,' not 'free beer'"

> Definitions via https://www.gnu.org/philosophy/free-sw.en.html

{{% note %}}
More of a ethical and philosophy movement than purely tech
{{% /note %}}

---

### Freedom 0

> The freedom to run the program as you wish, for any purpose (freedom 0).

{{% note %}}
Some licenses restrict the usages, such as being able to use it for military, medical purposes. This is no longer counted as free software.
{{% /note %}}

---

### Freedom 1

> The freedom to study how the program works, and change it so it does your computing as you wish (freedom 1). Access to the source code is a precondition for this.

---

### Freedom 2

> The freedom to redistribute copies so you can help others (freedom 2).

---

### Freedom 3

> The freedom to distribute copies of your modified versions to others (freedom 3). By doing this you can give the whole community a chance to benefit from your changes. Access to the source code is a precondition for this.
{{% /section %}}
