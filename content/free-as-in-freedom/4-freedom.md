---
weight: 4
---

{{% section %}}

# Let's talk about Freedom

---

{{< slide background-image="/images/free-as-in-freedom/dobby-free.gif" background-size="cover" >}}

---

### The 1960's

The decade of change.

{{< emoji e="📬" >}} &nbsp; {{< emoji e="💾" >}} &nbsp; {{< emoji e="🖥️" >}}

{{% note %}}

Software often came in the post

With sourcecode

Make it work to run on your hardware


- Mini skirts
- The pill
- Pride
- Star Wars
- Atari
- Rolling Stones
{{% /note %}}

---


### The 1980's

Proprietary was the norm

<div>
    <img src="/images/free-as-in-freedom/apple.svg" style="width:20%; display:inline-block; box-shadow:none; border:none; margin-right:2%">
        <span style='font-size:500%;'>
            💸
        </span>
    <img src="/images/free-as-in-freedom/windows.svg" style="width:20%; display:inline-block; box-shadow:none; border:none;">
</div>


{{% note %}}
Apples home computer came out

Software was expensive to make

Companies want to make money

Stopped shipping their sourcecode

{{% /note %}}

---

## Modifying source code
{{< emoji e="️️🖨️" >}}️{{< emoji e="️️➡️" >}} ️ {{< emoji e="️️📧" >}} 


{{% note %}}
No loger allowed to make changes

Set the printer to email the user when their job had finished

Emailed everyone if there was a Jam

New printer, wasn't allowed to modify the code

Fuming

{{% /note %}}


---

## The Free Software Foundation

### -1985-


{{% note %}}

The Free Software Foundation was founded

{{% /note %}}

---

{{< blockquote c="We call this free software because the user is free." a="Free Software Foundation">}}

{{% note %}}

User is the most important things

The user always deserve the changes, options, security updates

Don't protect the company or developer

Belongs to the person using the software

{{% /note %}}

---

### The Four Freedoms

<ul>
{{% fragment index="1" %}}<li style="margin-bottom: 3%;">The freedom to run the program as you wish, for any purpose.</li>{{% /fragment %}}

{{% fragment index="2" %}}<li style="margin-bottom: 3%;">The freedom to study how the program works, and change it so it does your computing as you wish.</li>{{% /fragment %}}

{{% fragment index="3" %}}<li style="margin-bottom: 3%;">The freedom to redistribute copies so you can help your neighbor.</li>{{% /fragment %}}

{{% fragment index="4" %}}<li style="margin-bottom: 3%;">The freedom to distribute copies of your modified versions to others. By doing this you can give the whole community a chance to benefit from your changes.</li>{{% /fragment %}}
</ul>

{{% note %}}
1. Do what you want with it
2. Look at the source code and modify
3. Share it around
4. Share your modified version
{{% /note %}}


---

{{< blockquote c="Think of free as in free speech, not as in free beer." a="Richard Stallman">}}

{{% note %}}

It's often cost free but the free in free software is about freedom

{{% /note %}}

---

## But how do we make money?

---

## Ask RedHat

They just sold to IBM for ~$34 billion

<div style="width:60%;margin:auto;">
    <img style="border:none; box-shadow:none;" src="/images/free-as-in-freedom/redhat-logo.png">
</div>


---

# So that's Free Software

{{% /section %}}
