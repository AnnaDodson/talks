---
weight: 5
---

{{% section %}}

# So what's Open Source?

---

## Pretty much the same thing

{{< emoji e="🤷‍" >}}

{{% note %}}

The terms are both used interchangable

Generally mean the same things

But they are different

Free - more ethically minded

{{% /note %}}


---

Software can be Open Source but not Free.

But if it's Free, it's Open Source

{{% note %}}

Good rule of thumb

Licesnce depending

{{% /note %}}

---

### The 1990's

The Netscape source code was released

(would later become the Firefox browser)

{{< emoji e="️️🦊" >}}️


{{% note %}}

Lots of developers got excited about usging this code for things

This was a huge oppurtunity for Free Software

Prompted the Open Source Initiative

{{% /note %}}

---

{{< blockquote c="To educate and advocate for the superiority of an open development process" a="Open Source Initiative">}}

{{% note %}}

Help users and developers engage

Open Source - a lot more pragmatic, less ethical

Free Software Foundation had a reputation of taking a hard stance by this point

Allowed commnities to grow and build things together for everyone

{{% /note %}}

---

<div>
    <img src="/images/free-as-in-freedom/linux-tux.svg" style="width:20%; display:inline-block; box-shadow:none; border:none;">
</div>

Create and improve source code by participating in an engaged community.

{{% note %}}

Linus Torvold was a supported of Open Source

He saw the benefits of open source

Believed it made better software

More secure

{{% /note %}}


---

<div>
    <img src="/images/free-as-in-freedom/gitlab-voting.png" style="border:none;">
</div>

[https://gitlab.com/gitlab-org/omnibus-gitlab/issues/1096](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/1096)


{{% note %}}

GitLab example of open source communities

Voting on features

Working together

GitLab - Open Source core

{{% /note %}}

---

<div>
    <img src="/images/free-as-in-freedom/ux-gitlab-screenshot.png" style="border:none;">
</div>

[https://about.gitlab.com/2019/07/26/quantifying-ux-positioning-of-the-clone-button/](https://about.gitlab.com/2019/07/26/quantifying-ux-positioning-of-the-clone-button/)


{{% note %}}

GitLab example of Open Source UX


{{% /note %}}


{{% /section %}}
