---
weight: 8
---

{{% section %}}

## Let's meet Karen

---

- Lawyer
- Degree in Engineering
- Executive Director of the Software Freedom Conservancy 
- General Counsel of the Software Freedom Law Center
- Co-organizes Outreachy, outreach program for women and people of color who are underrepresented in tech
- Pro bono counsel to the Free Software Foundation and GNOME

<p style="text-align:right">Karen Sandler</p>

{{% note %}}

Karen is awesome

We'd buy her a drink if she walked in

Progamming knowledge, lawyer, armed with high morals

{{% /note %}}

---

Karen has hypertrophic cardiomyopathy 

<small>
(genetic, the walls of her heart are very thick so it can be harder for the heart to pump blood)
</small>

{{< frag c="She had a pacemaker fitted" >}}

{{< frag c="In her 30's" >}}

{{< frag c="Then she got pregnant" >}}

{{% fragment %}}<span style="font-size: 500%;">🤦‍</span>{{% /fragment %}}

{{% note %}}

I heard Karen speak at FOSDEM

She shared her story about using Free Software

Forced to use proprietary pacemaker

NDA

She's in the rare position where she could make changes

Women are always edge cases

She just had to hope

{{% /note %}}

{{% /section %}}
