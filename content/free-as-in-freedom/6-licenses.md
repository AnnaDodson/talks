---
weight: 6
---

{{% section %}}

# But how do you know if it's FOSS?

---

## Check the License
{{< emoji e="📝‍" >}}

{{% note %}}

A great talk at Tech Notts all about licenses

If there's no license, you can't use it

{{% /note %}}

---

#### Copyright

Only the author/creator has rights to copy, distribute, adapt the work

#### Copyleft

The public retains freedom to copy, distribute, adapt the work, provided it is under the same copyleft terms


{{% note %}}

Free Software Foundation coined this term

Users keep the rights

If you make modifications - you can't change the license of the original work

Keeps it always open

{{% /note %}}

---

Software Licenses in Plain English

[https://tldrlegal.com](https://tldrlegal.com)

<div>
    <img style="border:none;" alt="tldr legal website screenshot" src="/images/free-as-in-freedom/tldrlegal-screenshot.png">
</div>


{{% note %}}

If you're not sure

Or you want to release your work

Great resource but not legal advice

{{% /note %}}

{{% /section %}}

