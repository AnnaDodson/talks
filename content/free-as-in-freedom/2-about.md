---
weight: 2
---

### And you are...?

<ul>
{{% fragment index="2" %}}<li>One of the organisers of WiT Notts</li>{{% /fragment %}}
{{% fragment index="3" %}}<li>I'm a Software Developer at Imosphere{{% /fragment %}}{{% fragment index="4" %}}<small>psst we're hiring</small></li>{{% /fragment %}}
{{% fragment index="5" %}}<li>Free and Open Source Software hobbiest</li>{{% /fragment %}}
</ul>

{{% note %}}

- I wasn't an organiser before I signed up

  Didn't used to know anyone

- We sell software as a service

 Healthcare apps - for healthcare data research

 - This is my hobby!

{{% /note %}}
