---
weight: 9
---

{{% section %}}

# What can we do?

---

## Use FOSS over Proprietary

There's usually always an open source alternative. Consider using it

{{< emoji e="‍💱" >}}

---

## Open Source Your Work

Are you doing something that others could benefit from?

{{< emoji e="‍🎁" >}}

---

## Become a Free and Open Source Advocate

Do you use FOSS at work? Champion contributing back - either time or money

{{< emoji e="‍⏳" >}}

---

## Free and Open Source Gives us a voice

Use it

{{< emoji e="‍📢" >}}


{{% /section %}}