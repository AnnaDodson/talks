---
weight: 3
---


{{% section %}}

# What do you mean by proprietary?

---

{{< blockquote c="Proprietary software is non-free computer software for which the software's publisher or another person retains intellectual property rights - usually copyright of the source code, but sometimes patent rights." a="Wikipedia">}}

