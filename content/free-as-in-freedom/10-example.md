---
weight: 10
---

{{% section %}}

# There's some amazing projects out there

---

<div>
    <img style="border:none; box-shadow:none; width: 10%" src="/images/free-as-in-freedom/openstreetmaps-logo.png">
</div>

OpenStreetMap is built by a community of mappers that contribute and maintain data about roads, trails, cafés, railway stations, and much more, all over the world.

[https://www.openstreetmap.org](https://www.openstreetmap.org)

{{% note %}}

Open Street Maps

Disaster areas

{{% /note %}}

---

<div>
<img style="border:none; box-shadow:none; width: 10%" src="/images/free-as-in-freedom/signdict-logo.svg">
</div>

SignDict is an open dictionary for sign language. Everyone is invited to join in. 

[https://signdict.org](https://signdict.org)

{{% note %}}

Sign Dict

Great resource

{{% /note %}}

---

<div>
<img style="border:none; box-shadow:none;" src="/images/free-as-in-freedom/wheelmap-logo.svg">
</div>

Wheelmap is a map for finding wheelchair accessible places.

The map works similar to Wikipedia: anyone can contribute and mark public places around the world according to their wheelchair accessibility. 

[https://wheelmap.org](https://wheelmap.org/)


{{% note %}}

Wheelchair map accessability

{{% /note %}}

---

<div>
<img style="border:none; box-shadow:none;" src="/images/free-as-in-freedom/a11y-logo.svg">
</div>


The A11Y Project

A community-driven effort to make web accessibility easier.

[https://a11yproject.com](https://a11yproject.com)

{{% note %}}

accessibility on the web

{{% /note %}}

---

<div style="width:10%;margin:auto;">
<img style="border:none; box-shadow:none;" src="/images/free-as-in-freedom/open-benches-logo.svg">
</div>

Open Benches

A map of 12,206 memorial benches - added by people like you

[https://openbenches.org](https://openbenches.org/)


{{% note %}}

Open Benches

{{% /note %}}

---

<div>
<img style="border:none; box-shadow:none;" src="/images/free-as-in-freedom/fashion-revolution-logo.svg">
</div>


Fashion Revolution

We want to unite people and organisations to work together towards radically changing the way our clothes are sourced, produced and consumed, so that our clothing is made in a safe, clean and fair way.

[https://www.fashionrevolution.org](https://www.fashionrevolution.org)

{{% note %}}

Mary Portas book - fashion vs football

{{% /note %}}

{{% /section %}}
