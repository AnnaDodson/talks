+++
title = "DevOps"
description = "A Hugo theme for creating Reveal.js presentations"
outputs = ["Reveal"]
transition = "slide"
transition_speed = "fast"
+++

# A talk about Dev Ops

~ by [@anna](https://annadodson.co.uk/) ~

# Pizza courtesy of @Imosphere
