+++
weight = 7
+++

{{% section %}}

# How To DevOps

---

## Spin it Up!
### (And back it up)

<div style="padding-top:2%; width:100%; margin:auto;">
<div style="display: flex; flex-wrap: wrap;">
  <div style="flex: 0 0 30%;">
    <img style="border:0;box-shadow:none;" src="/images/devops/azure-logo.jpeg">
  </div>
  <div style="flex: 0 0 30%;">
    <img style="border:0;box-shadow:none;" src="/images/devops/google-cloud-logo.png">
  </div>
  <div style="flex: 0 0 30%;">
    <img style="border:0;box-shadow:none;" src="/images/devops/aws-logo.png">
  </div>
</div>
</div>


{{% note %}}

- AWS Cloud Formation

- Google Cloud

- Azure

- On prem


{{% /note %}}

---

## Provisioning

<div style="padding-top:2%; width:100%; margin:auto;">
<div style="display: flex; flex-wrap: wrap;">
  <div style="flex: 0 0 30%;">
    <img style="border:0;box-shadow:none;" src="/images/devops/ansible.jpeg">
  </div>
  <div style="flex: 0 0 30%;">
    <img style="border:0;box-shadow:none;" src="/images/devops/puppet.png">
  </div>
  <div style="flex: 0 0 30%;">
    <img style="border:0;box-shadow:none;" src="/images/devops/chef-logo.png">
  </div>
</div>
</div>

{{% note %}}

Fancy word for installing stuff 

- Configuration Management
- Bash
- Chef
- Ansible
- Puppet

{{% /note %}}

---

## Multiple servers and data centres
### clusters

<div style="padding-top:2%; width:100%; margin:auto;">
<div style="display: flex; flex-wrap: wrap;">
  <div style="flex: 0 0 50%;">
    <img style="border:0;box-shadow:none;" src="/images/devops/kubernetes.png">
  </div>
  <div style="flex: 0 0 50%;">
    <img style="padding-top:2%;border:0;box-shadow:none;" src="/images/devops/docker-logo.jpeg">
  </div>
</div>
</div>

{{% note %}}

- AWS - elastic container service

- Kubernetes

- Docker Swarm

- Borg

{{% /note %}}


{{% /section %}}
