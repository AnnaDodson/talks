+++
weight = 6
+++

{{% section %}}

# Let's get continuous

{{% note %}}
Let's have a look at how to devOps in practise

{{% /note %}}

---

## Continuous Integration

- Small bite size chunks that work well together
- Merging work into the main branch often, multiple times a day
- Hour long branches

{{% note %}}

- Small manageable changes

{{% /note %}}

---

## Continuous Delivery

- Always have something to release:
- It’s tested
- Automated build

{{% note %}}

{{% /note %}}

---

## Continuous Deployment

- Deploying the code to customers:
  - Easily
  - Often
  - Automated
- Aim is no downtime or affect to users

{{% note %}}
 - Easily
 - Often
 - Automated

 With Confidence

{{% /note %}}

{{% /section %}}
