+++
weight = 9
+++

## Thanks for listening!

## Ask me questions

(or not - that's cool too)

I'll be hanging around for pizza, come say hi


<a href="https://twitter.com/anna_hax"><i class="fa fa-twitter" aria-hidden="true"></i> @anna_hax</a>

<a href="https://uk.linkedin.com/in/anna-dodson-28811ba9"><i class="fa fa-linkedin" aria-hidden="true"></i> Anna Dodson</a>

<a href="#"><i class="fa fa-slack" aria-hidden="true"></i>AnnaD</a>
