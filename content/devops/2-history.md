+++
weight = 2
+++

{{% section %}}

# Where did DevOps come from?

{{% note %}}
Deep breath
{{% /note %}}

---

# Let’s go back in time
<p style="font-size:128px;">
🦕
</p>

---

## Waterfall

- Long development cycle
- Tightly controlled change management
- Slow to release new features to customers
- Long feedback loop

{{% note %}}

- One computer per family

- No smart phones

- Systems develop
{{% /note %}}

---

## Agile

- Short development iterations
- Quicker to develop new features
- Shorter feedback loop

{{% note %}}

Everyone has as smart phone
Multiple devices
Online 24/7

{{% /note %}}

---


<div style="margin:auto;">
 <div style="display: flex; flex-wrap: wrap;">
  <div style="flex: 0 0 50%;">
    <img style="border:0;box-shadow:none;" src="/images/devops/linkedin-logo.png">
  </div>
  <div style="flex: 0 0 50%; margin-top:15%;">
    <ul>
      <li>Lots of new features</li>
      <li>Fragile infrastructure</li>
      <li>Needed to change</li>
    </ul>
  </div>
 </div>
</div>


{{% note %}}

- Stopped new development for 2 months
- Re wrote internal tools
- Re trained their release cycle
- Just before going public

{{% /note %}}


{{% /section %}}
