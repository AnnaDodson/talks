+++
weight = 8
+++

{{% section %}}

# So you wanna devOps??

---

{{< slide class="align-right" >}}

### Stuff to do

<div style="margin:auto; width:40%;">
 <div>
  <p style="text-align:left;">💻 Linux</p>
  <p style="text-align:left;">💻 Networking</p>
  <p style="text-align:left;">💻 Manage a server</p>
  <p style="text-align:left;">📘 SRE book</p>
  <p style="text-align:left;">📘 DevOps Handbook</p>
  <p style="text-align:left;">📘 Phoenix Project</p>

  <p style="text-align:left;">📘 https://cloud.google.com/blog/products/gcp/bringing-pokemon-go-to-life-on-google-cloud</p>
 </div>
</div>

{{% /section %}}
