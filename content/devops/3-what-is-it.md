+++
weight = 3
+++

{{% section %}}

# Ok, so DevOps is just Devs doing their own Ops?

{{% note %}}

- Deep breath

- Lets meet the team

{{% /note %}}

---

## This is Dev

<p style="font-size:128px;">
👩‍💻
</p>

{{% note %}}

-  Works on the product
-  Wants new features
-  Goes home at 5pm

{{% /note %}}

---

## This is Ops

<p style="font-size:128px;">
👩‍💻
</p>

{{% note %}}

- Manages the product in the wild
- Maintain stability at all times
- Change resistant
- Going to get called out at 2am if the site goes down

{{% /note %}}

---

## It’s all in the culture

Dev, meet Ops.
Ops, meet Dev
<p style="font-size:128px;">
🙋‍♀️ 🙋‍♀️
</p>

{{% note %}}

- A CULTURE
- Working as one
- Shared goals - a good feature rich stable product
- As is appropriate

{{% /note %}}

{{% /section %}}
