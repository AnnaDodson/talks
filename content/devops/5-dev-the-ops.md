+++
weight = 5
+++

{{% section %}}

# Bringing the Dev into Ops

{{% note %}}

- deep breath

- Bring the Engineering into site reliability

{{% /note %}}

---

## Shell scripts

<p style="font-size:128px;">
🐚
</p>

```

$ ssh production@critical-server
$ ./deploy-billion-servers.sh

```

{{% note %}}

- Automation starts with replacing manual steps with scripts

- You can still reproduce the steps by hand

- Need an audit trail

- Not scalable

{{% /note %}}

---

## Engineering solutions

<div style="padding-top:4%"></div>

| ServerId | CPU  | TimeUp              | ApplicationId | Memory |
|----------|------|---------------------|---------------|--------|
|1         | 85%  | 18.02.19t06.45.40   | 3456          | 196    |
|2         | 76%  | 14.02.19t08.20.00   | 763456        | 134    |


{{% note %}}

- Need to scale

- Borg - based on Kubernetes

- Clusters of tens of thousands of machines

- Automate fixing problems

- Route traffic to other data centres

{{% /note %}}

{{% /section %}}
