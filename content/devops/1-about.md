+++
weight = 1
+++

{{% section %}}

# about me

- I did a CS Masters here
- I was a member of HackSoc
- Now a .NET software engineer
- Work at Imosphere /i-mo-sphere/
- Interests include Linux, open source, devOps, automaton and getting involved in the tech community

---

# about this talk

- Where did it come from
- What is DevOps??
- Continuous stuff
- Examples of different tools
- How to DevOps in your own time

{{% /section %}}
