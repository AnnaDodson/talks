+++
weight = 4
+++

{{% section %}}

<div style="margin:auto;">
 <img style="border:0; box-shadow:none;" src="/images/devops/google-logo.jpeg">
</div>

{{% note %}}

- Google wrote the book on site reliability
- Lets take a look at how google does it

{{% /note %}}

---

## Service Level Agreement

- Services don’t need 100% uptime
- Realistic uptime goals
- New features vs stability
- Cost of resources

{{% note %}}

- Product owners, developers and SREs agree a realistic goal
- If it’s new, doesn’t need to be that reliable
- Data can be old as long as data is returned?
- Data must be up to date but can be late

{{% /note %}}

---

## Error Budget
<p style="font-size:128px;">
📈
</p>

{{% note %}}

- 100% uptime
- SREs and developers agree together
- Releases?

{{% /note %}}

---

## Monitor all the things

<p style="font-size:128px;">
🎛️
</p>

{{% note %}}

- What’s relevant to monitor?
- Don't monitor what isn't important
- Push alerts 

{{% /note %}}

---

## Spend it!

<p style="font-size:128px;">
💸
</p>

{{% note %}}
- Use it on deployments
- New features
- Over budget, halt new releases
{{% /note %}}

---


<div style="margin:auto;">
 <div style="display: flex; flex-wrap: wrap;">
  <div style="flex: 0 0 50%;">
    <img style="border:0;box-shadow:none;" src="/images/devops/pokemon.png">
  </div>
  <div style="flex: 0 0 50%; margin-top:15%;">
    <img style="border:0;box-shadow:none;" src="/images/devops/pokemon-go-graph.png">
  </div>
 </div>
</div>

{{% note %}}

- 50x the expected traffic

- Upgraded the container engine (kubernetes is based on) mid flight to allow more than a thousand extra nodes per container cluster - in flight

- Upgraded the Google cloud HTTPS load balancer to be more efficient at handling that level of traffic

{{% /note %}}

{{% /section %}}
