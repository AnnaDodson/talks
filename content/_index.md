+++
title = "Talks"
description = "Project Talks"
outputs = ["Reveal"]
[reveal_hugo]
custom_theme = "custom-theme.scss"
custom_theme_compile = true
[reveal_hugo.custom_theme_options]
targetPath = "css/custom-theme.css"
enableSourceMap = true
margin = 0.2
transition = "slide"
transition_speed = "fast"
+++

# Talks

[DevOps Talk](devops) @HackSoc UoN 19th Feb 2019

[Free and Open Source Software for Hacktoberfest](free-open-source-software) @HackSoc UoN 12th Oct 2018

[My Journey into Tech](getting-into-tech) @WiTNotts 7th March 2019

[Pokemon Go on Google Cloud](pokemon-lightning) @WiTLincs 11th June 2019

[Free as in Freedom, not as in Pizza](free-as-in-freedom) @WiTNotts 1st August 2019

[Noobs on Ubs](noobs-on-ubs) @PHPMinds 12th September & @OggCamp 19th October 2019

[Pokemon Go on Google Cloud](pokemon-lightning-take-two) @WiTNotts maybe?

[Diveristy and Inclusion - What can I do?](what-can-i-do) @DevOpsNotts 30th June 2020