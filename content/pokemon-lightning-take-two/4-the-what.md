+++
weight = 4
+++

### Pokémon Go launched

Australia and New Zealand

Where it proved to be quite the hit
  
<img style="width: 25%; border:0;box-shadow:none;" src="/images/devops/pokemon.png">


{{% note %}}

15 minutes after launching, traffic surged well past expectations
 
{{% /note %}}

