+++
weight = 5
+++

## Ready to Launch in America...

... and hit ten times the worst-case estimate....

{{% note %}}

 - 1X player traffic
 
 - With a worst-case estimate of 5X
 
 - Pokémon went to 50X the initial target

{{% /note %}}
