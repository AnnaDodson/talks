---
weight: 30
---

{{% section %}}

# Recommended Reading

{{< emoji e="‍📚" s="100" >}}[Invisible Women - Caroline Criado Perez](https://www.carolinecriadoperez.com/books)

{{< emoji e="‍📚" s="100" >}}[Slay in your Lane - Yomi Adegoke and Elizabeth Uviebinene](https://www.slayinyourlane.com/)


{{< emoji e="‍📚" s="100" >}}[Feminist Fight Club - Jessica Bennett](https://www.feministfightclub.com/)

{{< emoji e="‍🎙️" s="100" >}}[We Should all be Feminists - Chimamanda's Ted Talk](https://www.ted.com/talks/chimamanda_ngozi_adichie_we_should_all_be_feminists) (actually, anything by Chimamanda)

---

# References

[How advancing women’s equality can add $12 trillion to global growth](https://www.mckinsey.com/featured-insights/employment-and-growth/how-advancing-womens-equality-can-add-12-trillion-to-global-growth)

[Vague Feedback Is Holding Women Back by Shelley J. Correll and Caroline Simard.](https://hbr.org/2016/04/research-vague-feedback-is-holding-women-back)

[The Different Words We Use to Describe Male and Female Leaders by by David G. Smith	, Judith E. Rosenstein	and Margaret C. Nikolov](https://hbr.org/2018/05/the-different-words-we-use-to-describe-male-and-female-leaders)

---

<img style="border:none; box-shadow:none; width: 50%" src="/images/what-can-i-do/two-bunnies.jpg">

---

<img style="border:none; box-shadow:none; width: 20%" src="/images/what-can-i-do/stopwatch.png">

---

<img style="border:none; box-shadow:none; width: 70%" src="/images/what-can-i-do/easier-run.png">

---

<img style="border:none; box-shadow:none; width: 70%" src="/images/what-can-i-do/harder-run.png">


{{% /section %}}