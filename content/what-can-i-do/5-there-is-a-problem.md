---
weight: 5
---

{{% section %}}

# Let's acknowledge that there's a problem

The way things are currently working - aren't working.

---

## One group of people often get a lot of blame for this

{{% fragment %}}We can call them Not Diversity Educated{{% /fragment %}}

---

## For example

### Women do 75% of the worlds unpaid care work.

This affects travel needs.

 - Dropping off kids at school on the way to work
 - Taking an elderly relative to the doctor
 - Picking up groceries on the way home

This data isn't being actively collected because it's too hard.

{{% note %}}

Guess who doesn't benefit?

My point is, something being difficult isn't a good enough reason not to do it

{{% /note %}}

---

## So we all agree - there's definitely a problem

---

## We now have to accept some of it's our fault

### This is a tough one

{{% note %}}

This is hard to do.

But we need to accept we do it so we can start to make it better

If you accept you thought something, you can re-calibrate yourself. Question why you thought that.

{{% /note %}}

---

# Let's look at hiring

{{% note %}}
People are hired because the person hiring them thinks they're the best person for the job. What if the way they came to that conclusion is fundamentally flawed.
{{% /note %}}

---

## The best person for the job

{{% fragment %}}From the start of the Twentieth Century, the New York Philharmonic Orchestra had an average of 0 women musicians. There were a few exceptions.{{% /fragment %}}

{{% fragment %}}It was believed women weren't as good musicians. If they were, they would be hired.{{% /fragment %}}

{{% fragment %}}Following a lawsuit in 1970s, blind auditions were introduced.{{% /fragment %}}

{{% fragment %}}Today women represent between 45%-50% of musicians.{{% /fragment %}}


{{% note %}}

Can we accept the same is happening in tech?

{{% /note %}}

---

## Can we hold blind tech interviews?

{{% fragment %}}Check the job advert language.{% /fragment %}}

{{% fragment %}}Do you really need a CV?{{% /fragment %}}

{{% fragment %}}Personal projects are not required from a candidate.{{% /fragment %}}

{{% fragment %}}Question your unconscious bias every step of the way for every candidate.{{% /fragment %}}

{{% fragment %}}Display the salary{{% /fragment %}}

{{% fragment %}}Technical test? Really?{{% /fragment %}}

{{% fragment %}}Consider a quota{{% /fragment %}}

{{% note %}}

- Avoid ego centric terms
- Does a CV answer the questions you want answering? Just because you've always done it that way
- 75% of unpaid care work is done by women. When are they supposed to have the time? Don't be elitist! 
  - You wouldn't ask a doctor what they've been prescribing in their spare time
   - Having personal projects is great but you're cutting people out who don't have the spare time
- You should have clear goals and questions that you want to meet. It's just a conversation about everything you have in common?
- Don't contribute to the gender pay gap.
- At least let them chose the format. White board/take home

{{% /note %}}

{{% /section %}}