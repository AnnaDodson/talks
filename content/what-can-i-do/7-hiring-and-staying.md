---
weight: 7
---

{{% section %}}

## Quotas? Really?

{{% fragment %}}Aren't quotas discriminating against White men? Surely the best person for the job should get the job regardless of their gender/race/age...{{% /fragment %}}

{{% fragment %}}Yes they should! But the clearly aren't.{{% /fragment %}}

{{% fragment %}}Quotas can help readdress the balance.{{% /fragment %}}

{{% fragment %}}For example, aim to interview 5 men and 5 women.{{% /fragment %}}

{{% note %}}

- Good programmers are more likely to visit a manga porn site. Women are less likely to visit this website so their "suitability" ranking won't be as high. Gild
- You're then weeding out the mediocre and ensuring the best women are also considered.
- Indian, Pakistani, Bangladeshi = 9%
- African, Caribbean, Other Black = 7%

{{% /note %}}

---

## What if we can't find those applicants

{{% fragment %}}Have you thought about why?{{% /fragment %}}

{{% note %}}
It's not them, it's 100% you
{{% /note %}}

---

Part time and flexible working hours

{{% fragment %}}Childcare covered under expenses{{% /fragment %}}

{{% fragment %}}Good maternity and paternity cover{{% /fragment %}}

{{% fragment %}}Ensure personal safety{{% /fragment %}}

{{% fragment %}}Support for disabilities{{% /fragment %}}

{{% fragment %}}Do all the socials happen in the pub?{{% /fragment %}}

{{% fragment %}}Support religious ceremonies and celebrate different cultures{{% /fragment %}}

{{% fragment %}}Mandatory unconscious bias training{{% /fragment %}}

{{% note %}}
- Have I mentioned who does the most child care?
- Women can often negotiate at an existing company than at a new job. Encourages women to stay in jobs
- Don't put the onerous on women to fight for it, offer it and make it easy to sort

- If your holding events outside of work or people are travelling for work, who are you expecting to look after the children?
- Help people get to and from work safely. At events, make sure taxi's are offered and clear instruction on collection points and times
- Be clear on facilities and support available. My friend was given a form and able to order software that helps with her dyslexia at the same time as her mouse
- Have a bit of range!
- Especially for managers!
{{% /note %}}


{{% /section %}}