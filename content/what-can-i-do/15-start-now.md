---
weight: 15
---

{{% section %}}

## Start now

{{% note %}}
It didn't matter that I was in that room for someone to say something
{{% /note %}}

---

{{< blockquote c="An analysis of fifteen years of Supreme Court oral arguments found that 'men interrupt more than women, and they particularly interrupt women more than they interrupt other men" a="Caroline Criado Perez (2019), Invisible Women (pg 282)">}}

---

## Hostile work environments effect everyone

### but they effect minorities more

{{% note %}}

Hostile behaviors are noticed against minorities and is amplified even more for Black and Ethnic Minority women.

A confident privileged person will speak over someone they see as weaker and less important than themselves.

{{% note %}}

---

## Let's change the way we do things

Start practising now

{{% note %}}

Sexist office temperatures. Developed for 40yr old men in the 70s. 5deg colder for women.

If you're in a room full of people of the same demographic, you might have all learnt the behave in the same way. Not necessarily the best way.

If you're in a team without any visible minority people, you should start to practice being inclusive now.

Inclusive workplaces benefit everyone

{{% /note %}}

---

## We all love meetings

{{% fragment %}}Monitor and stop any interruptions{{% /fragment %}}

{{% fragment %}}Time box the speakers so everyone gets a say{{% /fragment %}}

{{% fragment %}}Move to unanimous decision making instead of majority based{{% /fragment %}}

{{% fragment %}}Listen to everyone{{% /fragment %}}

{{% fragment %}}Listen to everyone{{% /fragment %}}

{{% fragment %}}Listen to everyone{{% /fragment %}}

{{% note %}}

The last one is most important

I once watched someone in a meeting get their laptop out and replied to all their emails during a segment a woman developer was presenting. When she finished presenting, they put their laptop away.

Obviously they felt what she was saying wasn't important.

At promotion time, they felt she didn't contribute enough to the wider business.

{{% /note %}}


{{% /section %}}