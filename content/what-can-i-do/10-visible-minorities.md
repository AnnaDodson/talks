---
weight: 10
---

{{% section %}}

# What does diversity look like?

<div style="width:80%;margin: auto; padding: 2%">
    <div class="diversity-chart majority" style="width: 78%">Male</div><div class="diversity-chart minority" style="width: 18%">Female</div>
</div>
<div style="width:80%;margin: auto; padding: 2%">
    <div class="diversity-chart majority" style="width: 84%">White</div><div class="diversity-chart minority" style="width: 12%">Black</div>
</div>
<div style="width:80%;margin: auto; padding: 2%">
    <div class="diversity-chart majority" style="width: 60%">Able bodied</div><div class="diversity-chart minority" style="width: 36%">Disabled</div>
</div>
<div style="width:80%;margin: auto; padding: 2%">
    <div class="diversity-chart majority" style="width: 80%">Cis gendered</div><div class="diversity-chart minority" style="width: 16%">Transgender</div>
</div>
<div style="width:80%;margin: auto; padding: 2%">
    <div class="diversity-chart majority" style="width: 68%">Heterosexual</div><div class="diversity-chart minority" style="width: 28%">Homosexual</div>
</div>
<div style="width:80%;margin: auto; padding: 2%">
    <div class="diversity-chart majority" style="width: 88%">Everyone</div><div class="diversity-chart minority" style="width: 8%">You?</div>
</div>

{{% note %}}

- Doesn't necessarily look like anything

- Diversity data is not mandatory. A lot of people with disabilities will not declare this to their employer for fear of discrimination.

{{% /note %}}

---

## Can anyone guess which group of people have the best chance of enacting change?

<div style="width:80%;margin: auto; padding: 2%">
    <div class="diversity-chart majority" style="width: 78%">Majority</div><div class="diversity-chart minority" style="width: 18%">Minority</div>
</div>

{{% fragment %}}CARE {{% /fragment %}}{{% fragment %}} |  LISTEN {{% /fragment %}}{{% fragment %}} |  LEARN {{% /fragment %}}{{% fragment %}} |  ADAPT{{% /fragment %}}

{{% note %}}
If you identify with any of the examples on the left, your can help your friends on the right
{{% /note %}}

---

## Let's play a game

---

## During an incident response retro, the presenter makes a sexist remark.

<br>

### Which one of these do you think makes the biggest impact?

<br>

{{% fragment %}}**a.** Emails from some of the dev team, product owner, business analyst, scrum master and the site reliably engineer{{% /fragment %}}

{{% fragment %}}**OR**{{% /fragment %}}

{{% fragment %}}**b.** One single email from a minority demographic (that half the company don't respect anyway){{% /fragment %}}

{{% note %}}
I'm sure your companies all have clear, concise well advertised and established reporting structures in place
{{% /note %}}

---

### Don't speak for someone, speak for yourself

{{% fragment %}}Someone making fun of a person from a minority background should make you uncomfortable{{% /fragment %}}

{{% note %}}
- In a room with loads of men and someone were presenting a conference they had recently attended. The 5th or 6th talk they discussed was a womans talk.
- They stopped to have a good laugh at her clothes and appearance. What if next time one of the many men in the room had spoken up and said he didn't like it.
{{% /note %}}

{{% /section %}}