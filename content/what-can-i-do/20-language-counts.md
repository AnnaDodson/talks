---
weight: 20
---

{{% section %}}

## Listening for Language

---


<img style="border:none; box-shadow:none; width: 100%" src="/images/what-can-i-do/words-to-describe-women.png">

<div class="footnote-quote">Harvard Business Review. https://hbr.org/2018/05/the-different-words-we-use-to-describe-male-and-female-leaders</div>

{{% note %}}

- Found women got far more negative attributes
- women’s contributions aren't seen to connect to business outcomes
- women’s care-giving stereotype cause reviewers to more frequently attribute women’s accomplishments to teamwork

{{% /note %}}

---

She's too Emotional

{{% fragment %}}She needs to take a Step Back{{% /fragment %}}

{{% fragment %}}She's too Bossy{{% /fragment %}}

{{% fragment %}}She's so Abrasive{{% /fragment %}}

{{% fragment %}}She's completely Irrational{{% /fragment %}}

{{% fragment %}}She needs to Watch Her Tone{{% /fragment %}}

{{% fragment %}}She's Angry (specially reserved for Black Women){{% /fragment %}}

{{% note %}}

Some of these studies aren't conducted on enough of a sample size to be submitted for some journals

Because "women's Issues" aren't important. Despite Women taking up 50% of the population

{{% /note %}}

---

## When someone doesn't conform to your mental model

It's easy to reject it and react negatively

{{% note %}}
If something doesn't fit with the picture you have in your head

It's easy to reject it and say no - I don't accept that.

Acknowledge that's happening and questions it. It's the only way to update your mental model

{{% /note %}}


{{% /section %}}
