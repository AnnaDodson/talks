---
weight: 2000
---

**Title:**

### What can I do?

**Description:**

We know there's issues getting children interested in tech and encouraging students to study computer science but what other ways are there to encourage people to get into tech. And to _stay_ in tech.

I'm going to share some of the things I've learnt through being a developer and running tech events. Expect some examples and practical advice on things we can all be doing to improve diversity and inclusion in tech.
