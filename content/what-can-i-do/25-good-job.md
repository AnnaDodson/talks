---
weight: 25
---

{{% section %}}

# Something that's easy to forget

However good you are, there's still loads more you could be doing and at some point, you'll probably get it wrong. Even if it sucks - keep going.

---

{{< blockquote c="In a “full potential” scenario in which women play an identical role in labor markets to that of men, as much as $28 trillion, or 26 percent, could be added to global annual GDP by 2025." a="McKinsey (2019), How advancing women’s equality can add $12 trillion to global growth">}}

{{% /section %}}