+++
title = "What Can I Do?"
description = "Diversity and Inclusively - what can I do?"
outputs = ["Reveal"]
transition = "slide"
transition_speed = "fast"
+++

{{< slide transition="fade-out" transition-speed="fast" template="ubuntu" >}}

# What can I do?

~ by [@anna](https://annadodson.co.uk/) ~
