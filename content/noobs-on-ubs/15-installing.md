---
weight: 15
---

{{% section %}}

# My greatest struggle

## Installing stuff

{{< emoji e="‍‍‍🤷‍" s="500" >}}


{{% note %}}

Installing stuff quickly became my nemesis

I didn't understand how it worked

I needed to get my stuff done

Why read the docs when you can hack away at something for hours until it works

{{% /note %}}

---

<code style="font-size:110px" class="code">$ sudo</code>{{< emoji e="📦" s="500" >}}

{{% note %}}

Best way is to unzip it somewhere

Run as sudo

{{% /note %}}

---


## Advanced Package Tool

## `$ sudo apt update`

{{% note %}}
Command line interface for managing packages within Ubuntu
{{% /note %}}

---

## The Ubuntu package repositories

### Basically a database of available packages that have been tested

https://packages.ubuntu.com/disco/

{{% note %}}

So the system knows if there are new packages or updates to existing packages available

{{% /note %}}

---

## Update your local database

## `$ sudo apt update`

{{% note %}}
To update your local database to see if there's any new updates out there
{{% /note %}}

---

## Upgrade your packages

## `$ sudo apt upgrade`

{{% note %}}
To acutally upgrade the packages
{{% /note %}}

---

## Install a package

## `$ sudo apt install package`

{{% note %}}
This goes to the repositories and gets that package name
{{% /note %}}

---

## Need a specific version?

## `$ sudo apt install package=version`

---

## Need a newer version?

---

{{< emoji e="‍‍‍💩" s="700" >}}

{{% note %}}

Poop emoji gets real

Down a dark path of sudo everything

Unzip it in my home working directory make everythign executable

Do anything to get it would work

{{% /note %}}


---

## Personal Package Archive (PPA)

{{< blockquote c="A method of distributing software to users, without requiring developers to undergo the full process of distribution in the main ubuntu repositories" a="Ubuntu Wiki">}}

{{% note %}}

PPA is a way to get latest versions or packages not supported

{{% /note %}}

---

## Add the PPA to your package database

### Then use apt to install and upgrade

{{% note %}}

This gives apt the location of the package so it can go and get it

{{% /note %}}


---

## Adding a PPA allows full root access to your system
### Always trust who it's from
{{< emoji e="🚨" s="500" >}} 

{{% note %}}

When you add a PPA to your Software Sources, you're giving Administrative access (root) to everyone that can upload to that PPA.

Packages in PPAs have access to your entire system as they get installed (just like a regular package from the main Ubuntu Archive)

So always decide if you trust a PPA before you add it to your system.

{{% /note %}}

--- 


## Bit better than downloading and installing from a website

### Not as secure as installing from the main Ubuntu repositories

{{% note %}}

I used to not trust this at all

Not running whatever commands to add dodgy keys I don't know what are

I'd download zips and chmod them

{{% /note %}}

---

## It might break everything!
{{< emoji e="‍‍🔥" s="400" >}} 

{{% note %}}

Dependencies

Hardware

{{% /note %}}


{{% /section %}}
