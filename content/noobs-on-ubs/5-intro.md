---
weight: 5
---

{{% section %}}

# I'm the Noob

## and this is my story

{{< emoji e="‍📖" s="350" >}}

{{% note %}}

I'm the noob

Forever and always

{{% /note %}}

---

{{< emoji e="💻‍" s="900" >}}{{< emoji e="‍🐍" s="900" >}}

{{% note %}}

Career changed

Trying to get Python installed on Windows

Only solution - a whole new OS

{{% /note %}}

---

{{< emoji e="💻‍" s="900" >}}{{< emoji e="‍🔨" s="900" >}}

{{% note %}}

From Windows to Linux at uni

Very easy to set up

{{% /note %}}

---

<span style="font-size:900%;color:#e65e17;">‍❤  </span><img style="border:none; box-shadow:none; width: 16%" src="/images/supertinyicons/ubuntu.svg">

{{% note %}}

I went in to Ubuntu because I wanted control

And it to work

To know why it wasn't working

Still use windows at work

Got really good at re-installing

{{% /note %}}

{{% /section %}}
