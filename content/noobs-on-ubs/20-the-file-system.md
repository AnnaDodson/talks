---
weight: 20
---

{{% section %}}

# Ain't no Party
# Like a Linux Hierarchy
{{< emoji e="‍‍‍🤘" s="500" >}}

{{% note %}}

Time to get crazy

I struggled so much installing stuff cos I didn't know where stuff was supposed to go

{{% /note %}}

---

{{% fileTree %}}
* /
{{% /fileTree %}}

## The Root 
### everything is a file

---

{{% fileTree %}}
* bin
{{% /fileTree %}}

## Bin is for Binaries
### `pwd, mkdir, cat and systemctl`

{{% note %}}

Executables

The root level bin is for binaries

minimal system working and can be ran by any user.

Booting

Running

Restoring


{{% /note %}}

---

{{% fileTree %}}
* boot
{{% /fileTree %}}

## Boot is for booting
### static files for the bootloader.

{{% note %}}

Grub config

Kernel configuration settings

Enough said

{{% /note %}}

---

{{% fileTree %}}
* dev
{{% /fileTree %}}

## Dev is for devices.

{{% note %}}

Where physical devices are mounted.

The device files are interfaces for the systems device drivers.

Applications interact with the device driver the same as normal files

Using the standard input and output system calls.

{{% /note %}}

---

{{% fileTree %}}
* etc
{{% /fileTree %}}

## Etc like _Betsy_

### Configuration files for system wide applications

{{% note %}}

From the definition of et cetera, "additional odds and ends; extras".

Example, information for users: the groups, users, names, id's etc and the shadow file (the encrypted passwords of all the users on the system) also your hosts file.

These files are typically used by the system administrator.

{{% /note %}}

---

{{% fileTree %}}
* home
{{% /fileTree %}}

## Home is for _home_
###  For all your stuff like documents, pictures, projects etc.

{{% note %}}
All your stuff
{{% /note %}}

---

{{% fileTree %}}
* lib
{{% /fileTree %}}

## Lib is for Library
### System shared libraries for booting and running commands in the root filesystem.

{{% note %}}

Example, cyrptsetup library.

{{% /note %}}

---

{{% fileTree %}}
* media
{{% /fileTree %}}

## Media is for media
### Where media gets mounted to. If you plug in a USB for example.

{{% note %}}

USB

{{% /note %}}

---

{{% fileTree %}}
* opt
{{% /fileTree %}}

## Opt is for optional add-on software packages
### Nicely packaged apps with all their bits and bobs in one place

{{% note %}}
Add-on application software packages

All data for a package is within it's directory

{{% /note %}}

---

# The `/usr` Hierarchy

{{< emoji e="‍‍‍💡" s="500" >}}

{{% note %}}

Now let's have some real fun

{{% /note %}}

---

{{% fileTree %}}
* usr
{{% /fileTree %}}

## Usr is not for **user**.
_not_ user

### More like _user space_


{{% note %}}

Not for just your user stuff

Apparently it was once someones user directory when the `/bin` file got too big for one floppy disk and everything that needed to be there ready for the second disk went under `/usr`

Most other system files are duplicated here under `/usr/`.

It's for all system-wide, read-only files installed by (or provided by) the OS.

If you have multiple machines, they can all mount the same `/usr` directory and share the applications. This makes maintaining and updating pretty straight forward.

{{% /note %}}

---

{{% fileTree %}}
* usr
    * bin
{{% /fileTree %}}

## The primary directory for executables that are **not** needed for booting or repairing the system.

## `less`, `pkill`, `which`

{{% note %}}

Example Firefox or Libre Office

{{% /note %}}

---

{{% fileTree %}}
* usr
    * lib
{{% /fileTree %}}

## Libraries for packages installed that aren't system dependant.

{{% note %}}

Package libraries

Network manager

Python libraries

{{% /note %}}

---

{{% fileTree %}}
* usr
    * local
{{% /fileTree %}}

## locally installed software by the administrator

(probably you if it's your machine)

{{% note %}}

Doesn't get overwritten when the system software is updated

And again the file structure in here is duplicated _again_

- bin

- var

- lib
{{% /note %}}

---

{{% fileTree %}}
* usr
    * local
        * bin
{{% /fileTree %}}

## These binaries are for local programs

### Programs _not_ managed by the distribution package manager

{{% note %}}

For example, Jet Brains Rider

Isn't included in the official Ubuntu repositries.


{{% /note %}}

---

{{% fileTree %}}
* usr
    * share
{{% /fileTree %}}

## Share is for Sharing's Caring

### Architecture-independent data like man pages and locale information

Also ASCII character tables

{{% note %}}

Languages go here too

{{% /note %}}

---

# The Var Hierarchy
{{< emoji e="‍‍‍📝" s="500" >}}

---

{{% fileTree %}}
* var
{{% /fileTree %}}

## Var is for Variable Data Files

### Temp files like log files

{{% note %}}

`/var` is in the root file so that the `/usr` can be mounted as read only.

Anything that needs to write out is written out to `/var` now.

{{% /note %}}

---

{{% fileTree %}}
* var
    * cache
{{% /fileTree %}}

## Cache stuff

### Cached data from applications to save IO speed

{{% note %}}
These files can be deleted at any time by the user or removed when running low on disk space

All applications must be able to recover if they are removed.
{{% /note %}}

---

{{% fileTree %}}
* var
    * games
{{% /fileTree %}}

## Games are for playing

## Scores, levels and user specific game data

{{% note %}}
Needs writing out for the user as `/usr/` isn't writable
{{% /note %}}

---

# Want more?

## `$ man hier`

For more words to put into a search engine.



{{% /section %}}
