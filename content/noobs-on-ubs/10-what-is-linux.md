---
weight: 10
---

{{% section %}}

# If it's Linux, why is it called Ubuntu?

<img style="border:none; box-shadow:none; width: 10%" src="/images/supertinyicons/linux.svg">

{{% note %}}

Why is it called Ubuntu?

A distribution of Linux

{{% /note %}}

---

# It's based on Debian

<img style="border:none; box-shadow:none; width: 10%" src="/images/supertinyicons/debian.svg">

---

# Ok, so Linux... 


---

<img style="border:none; box-shadow:none; width: 40%" src="/images/noobs-on-ubs/the-colonel.png">

{{% note %}}
Managing resources
Network
Memory
Graphics
{{% /note %}}

---

## A bit like this...

<div style="width: 90%">
{{% fragment index="5" %}}<img style="width: 28%;" class="no-border no-box-shadow" src="/images/noobs-on-ubs/russian-dolls/russian-doll-desktop.svg" />{{% /fragment %}}
{{% fragment index="4" %}}<img style="width: 24%;" class="no-border no-box-shadow" src="/images/noobs-on-ubs/russian-dolls/russian-doll-ubuntu.svg" />{{% /fragment %}}
{{% fragment index="3" %}}<img style="width: 18%;" class="no-border no-box-shadow" src="/images/noobs-on-ubs/russian-dolls/russian-doll-debian.svg" />{{% /fragment %}}
{{% fragment index="2" %}}<img style="width: 14%;" class="no-border no-box-shadow" src="/images/noobs-on-ubs/russian-dolls/russian-doll-linux-gnu.svg" />{{% /fragment %}}
{{% fragment index="1" %}}<img style="width: 10%;" class="no-border no-box-shadow" src="/images/noobs-on-ubs/russian-dolls/russian-doll-linux-kernel.svg" />{{% /fragment %}}
</div>


{{% note %}}

Kernel - memory management, running processes, managing hardware devices

GNU/Linux - X Window System, BASH, C Compiler

Debian - package manager (APT), managing your applications, stability

Ubuntu - Usability, greater application support (proprietary apps), security set up for beginners

Desktop - clicky clicky looky looky

{{% /note %}}

---

<img style="border:none; box-shadow:none; width: 45%" src="/images/noobs-on-ubs/unity-logo.png">

At the time, it was Unity - now it's Gnome

{{% note %}}
It's what you see

The clicky clicky

So if you don't like it, you can change it
{{% /note %}}

---

<img style="border:none; box-shadow:none; width: 60%" src="/images/noobs-on-ubs/unity-3d-logo-cross.svg">

(the 3D modelling software)

{{% note %}}

Two very different things

{{% /note %}}

---

# Call me, maybe 

{{< emoji e="‍‍💁‍" s="700" >}} {{< emoji e="📞‍" s="500" >}}

{{% note %}}

Ubuntu handles System Calls

So firefox doesn't need to care what it's doing, just that it's doing it

{{% /note %}}

---

# Try a different flavour

## (like crisps)

{{% note %}}
Telling me what to do

Steve, Bill, Mum & Dad
{{% /note %}}

---

{{< slide class="big-list" >}}

{{< emoji e="🦜" s="100" >}} Budgie

{{< emoji e="🔮" s="100" >}} Kbuntu

{{< emoji e="🇨🇳" s="100" >}} Kylin

{{< emoji e="⚡" s="100" >}} Lubuntu

{{< emoji e="🕰" s="100" >}} Mate

{{< emoji e="📽" s="100" >}} Studio

{{< emoji e="🎨" s="100" >}} Xubuntu

{{% note %}}

Which is why you like the crisps?

{{% /note %}}

---

# Ok, makes total sense!

{{% note %}}

Do one small thing and do it well

I kind of get it now I've been using it a while

{{% /note %}}

{{% /section %}}
