---
weight: 30
---

# Thank you
### Let's chat later

{{< emoji e="👋" s="500" >}}


<br>

#### Special thanks to


<div style="display: inline-block; align=left; width: 48%;">
<ul>
    <li><a href="https://ubuntu.com/">Ubutnu</a></li>
    <li><a href="https://www.debian.org/">Debian</a></li>
    <li><a href="https://openmoji.org/">OpenEmoji</a></li>
<ul>
</div>
<div style="display: inline-block; align=left; width: 48%;">
<ul>
    <li><a href="https://github.com/edent/SuperTinyIcons)">Super Tiny Icons</a></li>
    <li><a href="(https://ikonate.com/">Ikonate</a></li>
    <li><a href="https://github.com/dzello/reveal-hugo">Reveal Hugo</a></li>
</ul>
</div>

