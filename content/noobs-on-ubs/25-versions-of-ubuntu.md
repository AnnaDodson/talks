---
weight: 25
---

{{% section %}}

# Versioning
## it's not you, it's me

{{< emoji e="💔" s="500" >}}

{{% note %}}

Laptop wouldn't wake from suspend

{{% /note %}}

---


## Warning!

### Using with anything other than Version 2.1.3 will cause unimaginable chaos to you and your family

{{< emoji e="💣" s="500" >}}

{{% note %}}

Number One lesson, google with the version number

{{% /note %}}

---

# Long Term Support

## 18.04.3 Bionic Beaver


{{% note %}}

Every two years a new Long Term Support release

You still get hardware upgrades that you can upgrade too

This is your enterprise production ready stable release

Because it's all tested!

{{% /note %}}

---

## Interim releases get newer features whilst still being somewhat stable

{{% note %}}
Do you want to wait for the newest release?
{{% /note %}}

---

# What if you want a new application?

## Snaps have got you covered...

---

## Snaps

{{< blockquote c="Snaps work across Linux on any distribution or version. Bundle your dependencies and assets, simplifying installs to a single standard command." a="Snapcraft.io">}}


{{% note %}}

Designed to improve releases of applications

Allow app developers to build one Linux package which will run on any distro

{{% /note %}}

---

# Snaps work independantly of the Ubuntu release cycle


## `$ sudo snap install package-name`

{{% note %}}

Use a snap to get the latest version without waiting for the latest Ubuntu release

Snaps are strictly confined in a secure box with only predefined points of access to the rest of the system
{{% /note %}}

---

# "I've heard snaps are _rubbish_"

### blah blah blah

{{% note %}}
Sometimes Linux people have opinions on stuff

Found some insecurities within snaps

Side affects
{{% /note %}}

---

# Getting stable releases with the newest features is hard

### More people testing bleeding edge releases, reporting bugs and enabling telemetry

{{% note %}}

Compatibility is hard

My job is to "make it work" - that's hard!

{{% /note %}}

{{% /section %}}
