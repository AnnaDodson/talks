+++
title = "Noobs on Ubs"
description = "Talk about using Ubuntu for beginners"
outputs = ["Reveal"]
transition = "slide"
transition_speed = "fast"
class = "ubuntu"
+++

{{< slide transition="fade-out" transition-speed="fast" template="ubuntu" >}}

<img style="border:none; box-shadow:none; width: 10%" src="/images/supertinyicons/ubuntu.svg">

# Noobs on Ubs


~ anna dodson ~
