---
weight: 27
---

{{% section %}}

# Where's the `Ctrl` + `Alt` + `Delete`?

{{% note %}}

Coming from Windows

What do you do when it goes wrong?

{{% /note %}}

---

## My quest to ~~look cool~~ be productive

{{< emoji e="📢" s="500" >}}

{{% note %}}

Sat in a workshop

Didn't know how to mute my laptop

Didn't know how to mirror my screen

{{% /note %}}

---

# If in doubt

<ul>
{{% fragment %}} <li>Try turning it off and on again</li>{{% /fragment %}}
{{% fragment %}} <li>Check what you're running `which`</li>{{% /fragment %}}
{{% fragment %}} <li>Check the version you're running `version`</li>{{% /fragment %}}
{{% fragment %}} <li>Ask someone! There's a huge community of people who have probably had this problem before</li>{{% /fragment %}}
{{% fragment %}} <li>Re-install! Keep everything backed up</li>{{% /fragment %}}
</ul>

{{% /section %}}

