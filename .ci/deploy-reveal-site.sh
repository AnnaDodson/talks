#!/usr/bin/env sh

# `-L` to resolve symlinks and output the files as they are
cp -LR "$1/out" "public/$1"
mv "public/$1/slides.html" "public/$1/index.html"
echo "<a href='$1/'>$2</a>" >> public/index.html
