#!/usr/bin/env sh

cd "$1"
# Install front-end dependencies
npm install
./node_modules/bower/bin/bower --allow-root install
# install rake+erubis
bundle install --path vendor/bundle
bundle exec rake
